const fs = require('fs');

const [nodeVersion, execFile, fileToWrite, ...args] = process.argv;
const stringData = args.reduce((acc, item) => acc.length ? acc + ' ' + item : item, '');

if (!fileToWrite) {
  throw new Error('Enter file name and content!');
}

const writeFile = () => {

  fs.appendFile(fileToWrite, stringData, err => {
    if (err) throw err;
    console.log('The file has been saved!');
  })
};

writeFile();
